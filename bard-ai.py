import os
import sys
import random


def main():
    # Podatność na inżynierię społeczną
    print("Podaj swoje imię:")
    name = input()
    print("Witaj, {}!".format(name))

    # Luka w zabezpieczeniach związanych z wejściem użytkownika
    print("Podaj swój numer PIN:")
    pin = input()
    print("Twój PIN to: {}".format(pin))

    # Podatność na ataki SQL Injection
    print("Podaj nazwę użytkownika bazy danych:")
    username = input()
    print("Podaj hasło bazy danych:")
    password = input()
    print("Połączenie z bazą danych zostało nawiązane jako użytkownik {}".format(username))

    # Luka w zabezpieczeniach związanych z zarządzaniem pamięcią
    for i in range(1000000):
        # Nie usuwanie obiektów z pamięci
        pass

    # Podatność na ataki typu denial-of-service
    while True:
        # Niekończący się loop
        pass


if __name__ == "__main__":
    main()
