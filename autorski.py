import os
# wczytanie danych z pliku o rozszerzeniu .xlsx
import openpyxl
data = openpyxl.load_workbook('data/data_processes.xlsx')
file = data.active

# tworzenie list
processes = []
burst_times = []
arrival_times = []

# ilość procesów
n = file.max_row - 1

# dopisanie danych do utworzonych list
for i in range(2, n+2):
    cell = file.cell(row=i, column=1)
    processes.append(cell.value)
    cell = file.cell(row=i, column=2)
    burst_times.append(cell.value)
    cell = file.cell(row=i, column=3)
    arrival_times.append(cell.value)

# sortowanie list według czasu przybycia danych procesów
for i in range(n):
    for j in range(i+1, n):
        if arrival_times[i] > arrival_times[j]:
            # zamiana miejscami ID procesów
            temp = processes[i]
            processes[i] = processes[j]
            processes[j] = temp
            # zamiana miejscami czasu trwania
            temp = burst_times[i]
            burst_times[i] = burst_times[j]
            burst_times[j] = temp
            # zamiana miejscami czasu przybycia
            temp = arrival_times[i]
            arrival_times[i] = arrival_times[j]
            arrival_times[j] = temp

# czas oczekiwania na rozpoczęcie wykonywania procesu 1; dla procesu 1 będzie to zawsze 0
waiting_times = [0]
# czas potrzebny do realizacji procesu 1; dla procesu 1 jest to tylko czas trwania tego procesu
turn_around_times = [burst_times[0]]
# moment, w którym ukończony został proces 1
completion_times = [arrival_times[0] + burst_times[0]]

# obliczenia dla kolejnych procesów
for i in range(1, n):
    a = completion_times[i - 1] - arrival_times[i]
    if a > 0:
        waiting_times.append(completion_times[i - 1] - arrival_times[i])    # czas oczekiwania na rozpoczęcie procesu
    else:
        waiting_times.append(0)
    completion_times.append(completion_times[i-1] + burst_times[i])         # moment, w którym zakończono proces
    turn_around_times.append(waiting_times[i] + burst_times[i])             # całkowity czas realizacji procesu

# obliczenie średniego czasu oczekiwania
sum_waiting_time = 0
for i in range(n):
    sum_waiting_time += waiting_times[i]
avg_waiting_time = sum_waiting_time / n

# obliczenie średniego czasu realizacji procesu
sum_turn_around_time = 0
for i in range(n):
    sum_turn_around_time += turn_around_times[i]
avg_turn_around_time = sum_turn_around_time / n

# zapisanie wyników do pliku tekstowego
file = 'results/fcfs_results.txt'

with open(file, 'a') as f:
    f.writelines(' Process ' + ' Arrival time ' + ' Burst time ' + ' Waiting time ' + ' Turn around time ')
    for i in range(n):
        f.writelines('\n    ' + str(processes[i]) + '\t\t     ' + str(arrival_times[i]) + '\t\t ' +
                     str(burst_times[i]) + '\t\t   ' + str(waiting_times[i]) + '\t\t\t   ' + str(turn_around_times[i]))

    f.writelines('\nAverage waiting time: ' + str(avg_waiting_time) + '.')
    f.writelines('\nAverage turn around time: ' + str(avg_turn_around_time) + '.\n')

print('Wyniki symulacji zostaly zapisane do pliku ' + file + '.')
os.system('PAUSE')
